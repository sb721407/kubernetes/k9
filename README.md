# SkillBox DevOps. Kubernetes. Часть 9. Мониторинг

SkillBox DevOps. Kubernetes. Часть 9. Мониторинг

--------------------------------------------------------

## Скриншоты к Заданию 1. Метрики для мониторинга кластера и их визуализация в Grafana

https://gitlab.com/sb721407/kubernetes/k9/-/blob/main/prom

## Скриншоты к Заданию 2. Парсинг логов

https://gitlab.com/sb721407/kubernetes/k9/-/blob/main/loki

